<?php
/**
 * Created by PhpStorm.
 * User: iclassroom
 * Date: 28/11/2018 AD
 * Time: 10:00
 */

use Illuminate\database\Seeder;


class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();
        Illuminate\Foundation\Auth\User::create(array(
            'name'     => 'kanyaratLortrakul',
            'username' => 'jingjung',
            'email'    => 'jingjungkai@gmail.com',
            'password' => Hash::make('jing2540'),
        ));
    }

}
