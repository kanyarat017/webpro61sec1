<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{number?}', function ($number=2) {
    //return 'this is a teting for a new route';
    /*
    for($i = 1;$i<=10;$i++){
        echo "$i* $number = ".$i* $number ."<br>";
    }
    */
    return view('book');
});

//Route::get('/test',function(){
 //  return 'this is a teting for a new route';
//});

Route::get('/','HomeController@showLogin');
