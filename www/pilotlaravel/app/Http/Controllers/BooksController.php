<?php

namespace App\Http\Controllers;

use App\Books;
use Illuminate\Http\Request;

class BooksController extends Controller
{
    /*
    function store(Request $request)
    {

        $book = new Books();
        $book->setAttribute("name",$request->name);
        $book->setAttribute("category",$request->category);
        $book->setAttribute("description",$request->description);
        if($book->save()){
            return true;
        }


    }*/
    function store(Request $request)
    {
        $book = new Books();
        if (Books::Create($request->all())) {
            return true;
        }
    }

    function update(Request $request,Books $books)
    {
        if($books->fill($request->all())){//ถ้ามีการเปลี่ยนแปลงแล้วจะให้มันเขียนลงไปใหม่ ทั้งอันที่เปลี่ยนแปลงและไม่เปลี่ยนแปลง(หมายถึงข้อมูล)
            return true;
        }
    }

    function index()
    {
        $books = Books::all();
        return $books;
    }

    function show(Books $books)
    {
        return $books;//return book ออกมาดู
    }

    function destroy(Books $books)
    {
        if($books->delete()){//ทำการลบข้อมูล
            return true;
        }
    }
}
