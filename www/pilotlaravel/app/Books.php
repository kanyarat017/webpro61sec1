<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $table = "my_books";
    protected $fillable = ['name','category','description'];
}
